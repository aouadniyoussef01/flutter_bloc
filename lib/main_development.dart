import 'package:bloc/bloc.dart';
import 'package:procaisse/app/app.dart';
import 'package:procaisse/bootstrap.dart';
import 'package:procaisse/observers/bloc_observer.dart';

void main() {
  Bloc.observer = CustomBlocObserver();
  bootstrap(App.new);
}
