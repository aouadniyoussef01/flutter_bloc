import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:procaisse/data/models/abstract_mode.dart';

import '../models/base_model.dart';
import '../models/user_model.dart';

class UserRepository implements IBaseModel {
  String userUrl = 'http://172.16.0.123:8000/api/getAllUsers';

  Future<BaseModel<List<UserModel>>> getUsers() async {
    Response response = await Dio().get(userUrl);

    BaseModel<List<UserModel>> baseModel = BaseModel.fromJson(response.data,UserRepository());
    return baseModel;
  }

  @override
  fromJson(List map) {
    return  (map as List).map((e) => UserModel.fromJson(e)).toList();
  }
}
