import 'package:procaisse/data/models/abstract_mode.dart';

class BaseModel<T> {
  String? status;
  bool? error;
  String? message;
  T? data;

  BaseModel({this.status, this.error, this.message, this.data,});

  BaseModel.fromJson(Map<String, dynamic> json,IBaseModel baseModel) {
    status = json['status'];
    error = json['error'];
    message = json['message'];
    data = baseModel.fromJson(json['data']);
  }
}
