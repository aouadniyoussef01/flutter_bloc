import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';

import '../interceptors/app_interceptors.dart';
import '../interceptors/logging_interceptors.dart';

@LazySingleton()
class DioConfiguration {
  factory DioConfiguration() => _singleton;

  DioConfiguration._internal();
  static final DioConfiguration _singleton = DioConfiguration._internal();

  final BaseOptions options = BaseOptions(
    headers: {'Content-Type': 'application/json', 'Accept': 'application/json', },
    connectTimeout: const Duration(minutes: 2),
    receiveTimeout: const Duration(minutes: 2),
  );

  Dio getDio() {
    final dio = Dio(options)
      ..interceptors.addAll(
        [
          LoggingInterceptors(),
        ],
      );

    return dio;
  }

  Dio getDioWithDynamicUrl(String baseUrl)
  {
    final dio  = Dio(BaseOptions(
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
        },
        connectTimeout:const Duration(minutes: 2),
        receiveTimeout: const Duration(minutes: 2),
        baseUrl: baseUrl,
    ),)..interceptors.addAll(
      [
        AppInterceptors(),
        LoggingInterceptors()
      ],
    );
    return dio;
  }
}
