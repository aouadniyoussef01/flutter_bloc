import 'dart:async';

import 'package:dio/dio.dart';
import 'package:logger/logger.dart';

class LoggingInterceptors extends Interceptor {
  @override
  FutureOr<dynamic> onRequest(RequestOptions options, RequestInterceptorHandler handler) async {
    Logger().w(
        'URL:  ${options.uri.toString()} \nQuery Params:${options.queryParameters.toString()} \nDATA: ${options.data.toString()}');
    super.onRequest(options, handler);
  }

  @override
  FutureOr<dynamic> onError(DioError err, ErrorInterceptorHandler handler) {
    Logger().e('ERROR: $err');
    super.onError(err, handler);
  }

  @override
  FutureOr<dynamic> onResponse(Response response, ResponseInterceptorHandler handler) {
    Logger().i('RESPONSE: $response');
    super.onResponse(response, handler);
  }
}
