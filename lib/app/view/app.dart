import 'package:beamer/beamer.dart';
import 'package:flutter/material.dart';
import 'package:procaisse/app/routing/login_location.dart';
import 'package:procaisse/l10n/l10n.dart';

class App extends StatelessWidget {
  App({super.key});
  final routerDelegate = BeamerDelegate(
    locationBuilder: BeamerLocationBuilder(
      beamLocations: [
        LoginLocation(),
      ],
    ),
  );

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      theme: ThemeData(
        appBarTheme: const AppBarTheme(color: Color(0xFF13B9FF)),
        colorScheme: ColorScheme.fromSwatch(
          accentColor: const Color(0xFF13B9FF),
        ),
      ),
      debugShowCheckedModeBanner: false,
      localizationsDelegates: AppLocalizations.localizationsDelegates,
      supportedLocales: AppLocalizations.supportedLocales,
      routerDelegate: routerDelegate,
      routeInformationParser: BeamerParser(),
    );
  }
}
