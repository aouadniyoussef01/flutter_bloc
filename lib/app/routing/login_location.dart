import 'package:beamer/beamer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:procaisse/bloc/login_bloc/login_bloc.dart';
import 'package:procaisse/presentation/login/login_screen.dart';

class LoginLocation extends BeamLocation<BeamState> {
  late LoginBloc _loginBloc;

  @override
  void initState() {
    super.initState();
    _loginBloc = LoginBloc();
  }

  @override
  void disposeState() {
    _loginBloc.close();
    super.disposeState();
  }

  @override
  void onUpdate() {

  }

  @override
  Widget builder(BuildContext context, Widget navigator) {
    return BlocProvider.value(
      value: _loginBloc,
      child: navigator,
    );
  }


  @override
  List<BeamPage> buildPages(BuildContext context, BeamState state) => [
    const BeamPage(
      key: ValueKey('login'),
      title: 'Login',
      child: LoginScreen(),
    ),

  ];

  @override
  List<String> get pathPatterns => ['/login'];
}