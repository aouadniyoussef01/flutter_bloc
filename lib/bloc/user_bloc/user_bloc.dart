import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:procaisse/bloc/user_bloc/user_event.dart';
import 'package:procaisse/bloc/user_bloc/user_state.dart';

import '../../data/repositories/user_repository.dart';



class UserBloc extends Bloc<UserEvent, UserState> {
  final UserRepository _userRepository;

  UserBloc(this._userRepository) : super(UserLoadingState()) {


    on<LoadUserEvent>((event, emit) async {
      emit(UserLoadingState());
        final response = await _userRepository.getUsers();
        emit(UserLoadedState(response.data!));
    });


  }
}
