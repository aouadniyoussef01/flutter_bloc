part of 'login_bloc.dart';

@immutable
abstract class LoginEvent {}

class LoginInit extends LoginEvent {}

class LoginLoading extends LoginEvent {}

class LoginError extends LoginEvent {}
