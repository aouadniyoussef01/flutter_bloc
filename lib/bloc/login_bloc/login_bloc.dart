import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';

part 'login_event.dart';
part 'login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  LoginBloc()
      : super(
          LoginInitial(),
        ) {
    on<LoginInit>((event, emit) {
      // TODO(username): implement event handler.
    });
    on<LoginLoading>(_onLoading);
    on<LoginError>(
      (event, emit) => {},
    );
  }
  Future<void> _onLoading(
    LoginLoading event,
    Emitter<LoginState> emit,
  ) async {
    debugPrint('Loading');
  }
}
